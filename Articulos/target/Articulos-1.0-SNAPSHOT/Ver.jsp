<%-- 
    Document   : Ver
    Created on : 9 jun. 2020, 20:01:30
    Author     : Damian
--%>

<%@page 
    contentType="text/html" pageEncoding="UTF-8"
    import ="java.sql.*"
    %>

<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pagina principal</title>
    </head>
    <body background="2.jpg">
        <STYLE type="text/css">
            H1 { text-align: center}    
            .boton {
                border: 1px solid #2e518b;
                padding: 10px;
                background-color: #3091DE; 
                color: #ffffff; 
                text-decoration: none; 
                text-transform: uppercase;
                font-family: 'Cambria', sans-serif; 
                border-radius: 50px;
            }
            div.scrollable
            {
                width:100%;
                height: 50px;
                margin: 0;
                padding: 0;
                overflow-y: auto
            }
            table th {
                color: #FFFFFF;
                //background-color: #f00;//Color de fondo de las celdas del encabezado
            }
        </STYLE>
        <div style="float:right"><a class="boton" href="Agregar.jsp">Agregar elemento</a> </div> 
        <div style="float:left"><a class="boton" href="Editar.jsp">Editar elemento</a> </div>
        <div style="float:right"><a class="boton" href="Eliminar.jsp">Eliminar elemento</a> </div>

    <center> <h1><font color="#FFFFFF" face="Helvetica" size="6">
            Tus articulos</font></h1>  </center>        

    <table align="center" width="85%" cellspacing="1" cellpadding="1" border="1">
        <tr><th>Id</th><th>Articulo</th><th>Caracteristicas</th><th>Marca</th><th>Precio</th></tr>

        <%
            Connection conex = null;
            Statement sta = null;
            ResultSet sql = null;
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                conex = (Connection) DriverManager.getConnection("jdbc:mysql://127.0.0.1/articulosd?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false", "root", "root");
                sta = conex.createStatement();
                sql = sta.executeQuery("select * from entrada");

                while (sql.next()) {
                    if (sql.getInt("visibilidad") != 0) {
        %>
        <tr>
        <font color="#FFFFFF">
        <th><%=sql.getString(1)%></th>
        <th><%=sql.getString(2)%></th>
        <th><div class="scrollable"><%=sql.getString(3)%></div></th>
        <th><%=sql.getString(4)%></th>
        <th><%=sql.getString(5)%></th>
    </tr>

    <%
                } else {
                    ;
                }
            }
        } catch (Exception e) {
            out.print("No tienes elementos en tu tabla");
        }
    %>
</table>
</tbody>
</body>
</html>
